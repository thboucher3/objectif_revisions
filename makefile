CC = gcc #Déclaration de variables
DEBUG = no
SRC = $(wildcard *.c)#Génération de la liste des fichiers sources
EXE = main
OBJ = $(SRC:.c=.o)
I = 10

ifeq ($(DEBUG), yes) #Condtions
$(info "Debug activé")
CFLAGS = -O0 -g -Wall -Wextra
else
CFLAGS = -O2 -Wall -Wextra
endif

LIB=-lm -lSANDAL2 -lSDL2 -lSDL2_ttf -lSDL2_image -lSDL2_mixer

all: $(OBJ)
	@echo "Compilation"
	@$(CC) -o $(EXE) $^ $(LIB)
#main.o : main.c hello.h
#	$(CC) -c $< $(CFLAGS)
#hello.o : hello.c
#	$(CC) -c $< $(CFLAGS)
#main.o : hello.h fichier1.h fichier2.h
%.o:%.c #Tous les fichiers .o: Tous les fichiers .c correspondant cependant pas de dépendances
	@$(CC) -c $< $(CFLAGS)
clean:
	@rm *.o
