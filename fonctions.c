#include <stdio.h>
#include "fonctions.h"
#include <stdlib.h>
#include <string.h>

void revisions(int argc, char ** argv){
	int num = 0;
	if (argc == 1){
		puts("Il faut mettre le numéro du programme de révision en argument");
		printf("ex : %s 1\n\n",argv[0]);
		puts("Voici la liste des programmes disponibles :");
		liste_pgrm();
	}
	else if (argc > 2){
		puts("trop d'arguments");
		puts("Seul le numéro du programme de révision est attendu");
		printf("ex : %s 1\n\n",argv[0]);
		puts("Voici la liste des programmes disponibles :");
		liste_pgrm();
	}
	else{
		sscanf(argv[1],"%d",&num);
		if (num < 1 || (num > 18 && num!=100)){
		puts("l'argument rentré ne correspond à aucun programme disponible\n");
		puts("Voici la liste des programmes disponibles :");
		liste_pgrm();
		}
		else intro(num);
	}
}

void intro(int x)
{	FILE * f = fopen("lpgrm2","r");
	FILE * fp = fopen("lnf","r");
	int compteur = 0;
	char s1[255];
	char s1p[255];
	char s[255];
	char * titre;
	char * nom_fichier_correspondant;
	int c = 1;
	int page = 1;
	
	
	while (compteur != x) {
		if (fgets(s1,255,f) == NULL) {
			puts("pb fgets s1");
			compteur=x-1;
		}
		if (fgets(s1p,255,fp) == NULL) {
			puts("pb fgets s1p");
			compteur=x-1;
		}
		compteur ++;
		if (compteur == 19) compteur = x;
	}
	titre = malloc(sizeof(char) * (strlen(s1) -1));
	if (!titre) {
		puts("pb allocation titre");
	}
	else strncpy(titre,s1,strlen(s1)-1);
	nom_fichier_correspondant = malloc(sizeof(char) * (strlen(s1p) -1));
	if (!nom_fichier_correspondant) {
		puts("pb allocation nom_fichier_correspondant");
	}
	else strncpy(nom_fichier_correspondant,s1p,strlen(s1p)-1);
	
	printf(" PROGRAMME DE REVISIONS #%d : %s\n",x,titre);
	while (c){
		sprt();
		printf("Démarrer la lecture depuis le début (O/n) : ");
		if (suite()==0) {
			printf("Choix de la page de démarrage : ");
			if (fgets(s,255,stdin) == NULL) {
			puts("pb fgets s");
			c = 0;
			}
			else sscanf(s,"%d",&page);
		}
		if (c!=0) {
			sprt();sprt();
			lecture(nom_fichier_correspondant,page);
			sprt();
			printf("continuer à lire ce fichier (O/n) : ");
			if (suite() == 0) c=0;
			else page = 1;
		}
	}
	
	free(titre); free(nom_fichier_correspondant);
	fclose(f); fclose(fp);
}

void liste_pgrm(void){
	FILE * f = fopen("lpgrm","r");
	int lecture = 1;
	char s[255];
	while (lecture){
		if (fgets(s,255,f) == NULL) {
			puts("pb fgets lpgrm");
			lecture = 0;
		}
		if (strcmp(s,"FIN\n") == 0) lecture = 0;
		else printf("%s",s);
	}
	fclose(f);
}

int suite(void){
	int res = 2;
	char s[255];
	
	while(res == 2) {
		if (fgets(s,255,stdin) == NULL) {
			puts("pb fgets suite");
			res=0;
		}
		if (strcmp(s,"\n") == 0 || strcmp(s,"O\n") == 0 || strcmp(s,"o\n") == 0 || strcmp(s,"oui\n") == 0) res = 1;
		else if (strcmp(s,"n\n") == 0 || strcmp(s,"N\n") == 0 || strcmp(s,"non\n") == 0) res = 0;
		else printf("réponse invalide, réessaie : ");
	}
	return res;
}

void sprt(void){
	puts("********************************************************************************");
}

int continuer(void){
	int res;
	sprt();
	printf("page suivante (O/n) : ");
	res = suite();
	
	return res;
}

void lecture(char * nom_fichier, int page){
	if (page < 1) puts("le numéro de page rentré est invalide");
	else{
		FILE * f = fopen(nom_fichier,"r");
		char s[255];
		int l = 1;
		int compteur = 1;
		int debut = 1;
		while (l){
			while (l){
			if (fgets(s,255,f) == NULL) {
				puts("pb fgets l");
				l=0;
			}
			if (l!=0 && debut && compteur >= page) {
				printf("début page %d\n",compteur);
				debut = 0;
			}
			if (l!=0 && strcmp(s,"FIN\n") == 0) {
				l=0;
				if (compteur < page) puts("le numéro de page rentré est invalide");
				else printf("fin page %d\n",compteur);
			}
			else if(l!=0 && strcmp(s,"#\n") == 0) {
				if (compteur >= page){
					debut = 1;
					printf("fin page %d\n",compteur);
					if (continuer()) puts("page suivante nous voilà !!");
					else {
						puts("retour au menu du programme de révisions");
					l=0;
					}
					sprt();
				}
				compteur++;
			}
			else if(l!=0 && compteur >= page) printf("%s\n",s);
			}
		}
		fclose(f);
	}
}
