#ifndef fonctions_h
#define fonctions_h

/*
 * Fonction principale
 */
void revisions(int argc, char ** argv);

/*
 * Fonction de présentation
 */
void intro(int x);

/*
 * Fonction qui affiche la liste des programmes disponibles
 */
void liste_pgrm(void);

/*
 * Fonction qui renvoie 1 si l'utilisateur entre o, O ou entrée et 0 sinon
 */
int suite(void);

/*
 * Fonction qui affiche un séparateur
 */
void sprt(void);

/*
 * Fonction qui demande à l'utilisateur s'il souhaite accéder à la page suivante ou retourner au menu
 */
int continuer(void);

/*
 * Fonction qui lit le contenu d'un fichier texte
 */
void lecture(char * nom_fichier, int page);
#endif
