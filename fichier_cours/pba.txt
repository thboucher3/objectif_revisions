
  ** à remplir **
  
  Dans le fichier pba.txt, vous pouvez rajouter tout le contenu concernant les proba
  
  Il suffit d'utiliser le symbole "#" pour créer une nouvelle page
  
  comme ceci :
  
#

  pour marquer la fin du contenu à lire, il faut utiliser le symbole "FIN"
  
  comme ceci :

FIN

Ce qui est écrit après le symbole "FIN" n'est pas lu !
